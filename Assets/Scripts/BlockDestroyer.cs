﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockDestroyer : MonoBehaviour {

    public ScoreManager SM;
    public int scoreValue = 2;
    public int finalNumber;
    //public Rigidbody2D rb;
    void Start()
    {
        SM = FindObjectOfType<ScoreManager>();
        GetComponent<Rigidbody2D>().gravityScale += Time.timeSinceLevelLoad / 20f;
    }	

	
	void Update () {
		if(transform.position.y < -8f)
        {
            SM.scoreCount += scoreValue;
            Destroy(gameObject);
            

        }
	}
}
