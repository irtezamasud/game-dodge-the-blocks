﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    public float speed = 20f;
        private Rigidbody2D rb;
            public float mapWidth = 5f;
            public GameObject boxSpawn;
                 public Animator playerAnim;
    //public GameManager gameMangeSc;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        playerAnim = GetComponent<Animator>();

    }
    void FixedUpdate() {

            float move = Input.GetAxis("Horizontal") * Time.deltaTime * speed;
            playerAnim.SetFloat ("HSpeed", move );
            Vector2 newPosition = rb.position + Vector2.right * move;
            newPosition.x = Mathf.Clamp(newPosition.x, -mapWidth, mapWidth);
            rb.MovePosition(newPosition);
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        FindObjectOfType<GameManager>().EndGame();
    }

}
