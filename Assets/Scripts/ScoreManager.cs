﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {
    public Text Score;
    public Text Highscore;
    public int scoreCount;
	
	void Start () {
        Highscore.text = PlayerPrefs.GetInt("Hscore", 0).ToString();
    }
	
	
	void Update () {
        Score.text = "Score: " + scoreCount; //Updating Score
        Highscore.text = "HighScore: " + scoreCount; //Updating HighScore

        
        if (scoreCount> PlayerPrefs.GetInt("Hscore", 0))
        {
            PlayerPrefs.SetInt("Hscore", scoreCount);
            Highscore.text = scoreCount.ToString();
        }
        

	}
}
