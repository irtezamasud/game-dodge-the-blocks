﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public float SlowFactor = 10f;
	public void EndGame ()
    {
        StartCoroutine(RestartLevel());
    }

    IEnumerator RestartLevel ()
    {
        Time.timeScale = 1f / SlowFactor;
        Time.fixedDeltaTime = Time.fixedDeltaTime / SlowFactor;
        yield return new WaitForSeconds(1.3f/ SlowFactor);
        Time.timeScale = 1f;
        Time.fixedDeltaTime = Time.fixedDeltaTime * SlowFactor;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.LoadLevel(0);
        }
    }
}
